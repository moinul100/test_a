import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./Components/Home/Home";
import AddFood from "./Components/AddFood/AddFood";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>

        <Route path="/addFood">
          <AddFood />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
