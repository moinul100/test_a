import { useEffect, useState } from "react";
import io from "socket.io-client";
import { toast } from "react-toastify";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import notificationSound from "../Audio/ihpne.mp3"; // Replace with your audio file

const audio = new Audio(notificationSound);

const socket = io("http://localhost:8000");

const AddProductSocketIo = () => {
  const [food, setFood] = useState("");
  const [currentUserEmail, setCurrentUserEmail] = useState("");

  useEffect(() => {
    setCurrentUserEmail("admin@gmail.com");

    socket.on("foodAdded", (data) => {
      // Display a notification only if the current user is "admin@gmail.com"
      if (currentUserEmail === "admin@gmail.com") {
        toast.success(`New food added: ${data.name}`);
        // Play the notification sound
        audio.play();
      }
    });
  }, [currentUserEmail]);

  const handleAddFood = async () => {
    // Make a POST request to your backend API to add food to the database
    const response = await fetch("http://localhost:8000/addFood", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name: food }),
    });

    if (response.status === 200) {
      setFood(""); // Clear the input field
    } else {
      // Handle error if needed
    }
  };
  return (
    <div>
      <h1>Real-time Food App</h1>
      <input
        type="text"
        placeholder="Food Name"
        value={food}
        onChange={(e) => setFood(e.target.value)}
      />
      <button onClick={handleAddFood}>Add Food</button>

      <ToastContainer position="bottom-right" />
    </div>
  );
};

export default AddProductSocketIo;
