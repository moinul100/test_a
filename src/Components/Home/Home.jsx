import { Link } from "react-router-dom";
const Home = () => {
  return (
    <div>
      <Link to="/addFood">
        <button className="btn btn-primary">add food</button>
      </Link>
    </div>
  );
};

export default Home;
