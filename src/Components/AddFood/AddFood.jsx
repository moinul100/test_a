// import { useRef, useState } from "react";
// import "./AddFood.css";

// const AddFood = () => {
// const nameRef = useRef();
// const [error, setError] = useState(null);
// const [messgae, setMessage] = useState("");

// const handleSubmit = async (event) => {
//   // Prevent the default form submission behavior
//   event.preventDefault();

//   // Extract the food data from the form input
//   const foodData = {
//     name: nameRef.current.value,
//   };
//   console.log(foodData, "foodData");
//   try {
//     // Define the API endpoint
//     const url = "http://localhost:8000/addFood";

//     // Configure the HTTP request options
//     const options = {
//       method: "POST",
//       body: JSON.stringify(foodData),
//       headers: {
//         "Content-Type": "application/json; charset=UTF-8",
//       },
//     };

//     // Send the POST request to the server
//     const response = await fetch(url, options);

//     // Check if the response indicates success
//     if (!response.ok) {
//       throw new Error("Failed to add food item.");
//     }

//     // Clear the input field and refresh the page
//     nameRef.current.value = "";
//     // window.location.reload();
//     setMessage("data added");
//   } catch (error) {
//     // Handle errors by displaying an error message and logging the error
//     setError("An error occurred while adding the food item.");
//     console.error("Error:", error);
//   }
// };

//   return (
// <>
//   <h1>{messgae}</h1>
//   <form className="row" onSubmit={handleSubmit}>
//     <div className="col-6 mb-4">
//       <label htmlFor="name" className="form-label">
//         Name
//       </label>
//       <input type="text" className="form-control" id="name" ref={nameRef} />
//     </div>
//     <div className="col-12">
//       <button type="submit">Submit</button>
//       {error && <p className="error">{error}</p>}
//     </div>
//   </form>
// </>
//   );
// };

// export default AddFood;

// import { useState, useEffect, useRef } from "react";
// import io from "socket.io-client";

// const socket = io("http://localhost:8000");

// const AddFood = () => {
//   const [foodAdded, setFoodAdded] = useState(null);

//   useEffect(() => {
//     // Listen for the "foodAdded" event from the backend
//     socket.on("foodAdded", (food) => {
//       setFoodAdded(food);
//     });

//     return () => {
//       // Disconnect the socket when the component unmounts
//       socket.disconnect();
//     };
//   }, []);

//   const nameRef = useRef();
//   const [error, setError] = useState(null);
//   const [messgae, setMessage] = useState("");

//   const handleSubmit = async (event) => {
//     // Prevent the default form submission behavior
//     event.preventDefault();

//     // Extract the food data from the form input
//     const foodData = {
//       name: nameRef.current.value,
//     };
//     console.log(foodData, "foodData");
//     try {
//       // Define the API endpoint
//       const url = "http://localhost:8000/addFood";

//       // Configure the HTTP request options
//       const options = {
//         method: "POST",
//         body: JSON.stringify(foodData),
//         headers: {
//           "Content-Type": "application/json; charset=UTF-8",
//         },
//       };

//       // Send the POST request to the server
//       const response = await fetch(url, options);

//       // Check if the response indicates success
//       if (!response.ok) {
//         throw new Error("Failed to add food item.");
//       }

//       // Clear the input field and refresh the page
//       nameRef.current.value = "";
//       // window.location.reload();
//       setMessage("data added");
//     } catch (error) {
//       // Handle errors by displaying an error message and logging the error
//       setError("An error occurred while adding the food item.");
//       console.error("Error:", error);
//     }
//   };
//   return (
//     <div>
//       <h1>Real-time Notifications</h1>
//       {foodAdded && <p>New food added: {foodAdded.name}</p>}
//       {/* Render your component content here */}

//       <>
//         <h1>{messgae}</h1>
//         <form className="row" onSubmit={handleSubmit}>
//           <div className="col-6 mb-4">
//             <label htmlFor="name" className="form-label">
//               Name
//             </label>
//             <input
//               type="text"
//               className="form-control"
//               id="name"
//               ref={nameRef}
//             />
//           </div>
//           <div className="col-12">
//             <button type="submit">Submit</button>
//             {error && <p className="error">{error}</p>}
//           </div>
//         </form>
//       </>
//     </div>
//   );
// };

// export default AddFood;

// import { useEffect, useState } from "react";
// import { messaging } from "../../firebase.config"; // Import the messaging instance from your configuration file

// const AddFood = () => {
//   const [fcmToken, setFcmToken] = useState(null);

//   useEffect(() => {
//     // Request notification permission and get FCM token
//     messaging
//       .requestPermission()
//       .then(() => {
//         console.log("Notification permission granted.");
//         return messaging.getToken();
//       })
//       .then((token) => {
//         console.log("FCM Token:", token);
//         setFcmToken(token); // Store the token in state or send it to your server
//       })
//       .catch((error) => {
//         console.error("Error getting FCM token:", error);
//       });
//   }, []);

//   return (
//     <div>
//       {fcmToken ? (
//         <p>FCM Token: {fcmToken}</p>
//       ) : (
//         <p>Waiting for FCM Token...</p>
//       )}
//     </div>
//   );
// };

// export default AddFood;

// import React, { useEffect } from "react";
// import firebase from "firebase/app";
// import "firebase/messaging";

// const firebaseConfig = {
// apiKey: "AIzaSyAL7kAdnVItOYFr4o6rCQ-p3bmtLZEfcY8",
// authDomain: "dishco-c3698.firebaseapp.com",
// projectId: "dishco-c3698",
// storageBucket: "dishco-c3698.appspot.com",
// messagingSenderId: "1074663610277",
// appId: "1:1074663610277:web:c98576cc14a3d994103a4c",
// measurementId: "G-X22NDNJE64",
// };

// firebase.initializeApp(firebaseConfig);
// const messaging = firebase.messaging();

// const requestNotificationPermission = async () => {
//   try {
//     await Notification.requestPermission();
//     console.log("Notification permission granted.");
//   } catch (error) {
//     console.error("Error requesting notification permission:", error);
//   }
// };

// const getFCMToken = async () => {
//   try {
//     const token = await messaging.getToken();
//     if (token) {
//       console.log("FCM token:", token);
//       // Send this token to your server for later use.
//     } else {
//       console.log("No FCM token available.");
//     }
//   } catch (error) {
//     console.error("Error getting FCM token:", error);
//   }
// };

// const App = () => {
//   useEffect(() => {
//     requestNotificationPermission();
//     getFCMToken();
//   }, []);

//   return (
//     <div>
//       <h1>FCM Token Retrieval Example</h1>
//       {/* Your React components */}
//     </div>
//   );
// };

// export default App;

//this code should be on app.js/index.js
import React, { useEffect, useState } from "react";
import firebase from "firebase/app";
import "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyAL7kAdnVItOYFr4o6rCQ-p3bmtLZEfcY8",
  authDomain: "dishco-c3698.firebaseapp.com",
  projectId: "dishco-c3698",
  storageBucket: "dishco-c3698.appspot.com",
  messagingSenderId: "1074663610277",
  appId: "1:1074663610277:web:c98576cc14a3d994103a4c",
  measurementId: "G-X22NDNJE64",
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

const AddFood = () => {
  const handleGetTokenClick = async () => {
    try {
      await messaging.requestPermission();
      const token = await messaging.getToken();

      console.log(token);

      // Now that you have the FCM token, you can send it to your backend
      // Make a POST request to your server with the token (you may need an API endpoint for this)

      // Example: Sending the token to your server
      await fetch("http://localhost:8000/send", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ token }),
      });

      // Once the token is sent to the server, you can listen for incoming messages
      const unsubscribe = messaging.onMessage((payload) => {
        console.log("Message received:", payload);
        // Display the incoming message as a notification
        const { title, body } = payload.notification;
        new Notification(title, { body });
      });

      // Remember to unsubscribe from the listener when it's no longer needed
      return () => {
        unsubscribe();
      };
    } catch (error) {
      console.error("Error granting notification permission:", error);
    }
  };

  return (
    <div>
      <h1>FCM Token Retrieval Example</h1>
      <button onClick={() => handleGetTokenClick()}>Get FCM Token</button>
      {/* Your React components */}
    </div>
  );
};

export default AddFood;
