importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js"
);

const firebaseConfig = {
  apiKey: "AIzaSyAL7kAdnVItOYFr4o6rCQ-p3bmtLZEfcY8",
  authDomain: "dishco-c3698.firebaseapp.com",
  projectId: "dishco-c3698",
  storageBucket: "dishco-c3698.appspot.com",
  messagingSenderId: "1074663610277",
  appId: "1:1074663610277:web:c98576cc14a3d994103a4c",
  measurementId: "G-X22NDNJE64",
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.onBackgroundMessage((payload) => {
  console.log(
    "[firebase-messaging-sw.js] Received background message ",
    payload
  );
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: payload.notification.image,
  };

  self.registration.showNotification(notificationTitle, notificationOptions);
});
